# README

Netflix - Arquetipo de pruebas automatizadas E2E usando la herramienta cypress

## Complementos

|**Visual Studio Code**|**NodeJS**|**Cypress**|
| :----: | :----: | :----:  |
|[<img width="50" height="50" src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Visual_Studio_Code_1.35_icon.svg/768px-Visual_Studio_Code_1.35_icon.svg.png?20210804221519">](https://code.visualstudio.com)|[<img height="60" src="https://cdn.freebiesupply.com/logos/large/2x/nodejs-1-logo-png-transparent.png">](https://nodejs.org/en)|[<img height="50" src="https://upload.wikimedia.org/wikipedia/commons/a/a4/Cypress.png">](https://www.cypress.io/)|
> **NOTA**:
> * Una vez obtenido Visual Studio Code es opcional (pero recomendable) instalar los plugins de Gherkin y Cucumber. (*[Guia de manejo de extensiones en Visual Studio](https://code.visualstudio.com/docs/editor/extension-marketplace)*)
>

## Ejecución local

Clonar el proyecto

```bash
  git clone https://gitlab.com/luis.mass/cypress-bc-challenge.git
```

Entrar al directorio del proyecto

```bash
  cd cypress-bc-challenge
```

Instalar las dependencias

```bash
  npm install
```

### Comandos

Existen 2 modos para ejecutar las pruebas

Abriendo la interfaz de cypress (por este método se elige el feature en la intefaz)
```bash
  npm run cypress:open 
```

> :warning: **IMPORTANTE**:
> Cuando se ejecuta el comando para la interfaz grafica de Cypress **no** se genera un reporte html, por lo cual para tener un reporte después de la ejecucion de las pruebas se debe ejecutar el comando **npm run cypress:run**

En 2do plano por consola
```bash
  npm run cypress:run 
```

Para ejecutar todos los escenarios dentro de un archivo .feature especifico
```bash
  npm run cypress:run -- --spec cypress/e2e/**NombreFeature**.feature
```

Para ejecutar un tag específico
```bash
  npx cypress run --env TAGS="@regression"
```

Por defecto cypress ejecuta las puebas usando el navegador interno **Electron**, en caso de querer  ejecutar en otro navegador
```bash
  npm run cypress:run -- --browser nombreBrowser
```
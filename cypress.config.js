const { defineConfig } = require("cypress");


module.exports = defineConfig({
  e2e: {
    setupNodeEvents(on, config) {
      return require('./cypress/plugins/index.js')(on, config)
    },
    env: {
      username: '734746',
      password: '10df2f32286b7120MS00LTY0NzQzNw==30e0c83e6c29f1c3',
      TAGS: "not @skip"
    },
    baseUrl: 'https://tasks.evalartapp.com/automatization/',
    specFolder: 'cypress/e2e',
    specPattern: 'cypress/e2e/*.feature',
  }
});
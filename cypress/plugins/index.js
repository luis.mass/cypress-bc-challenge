const cucumber = require('cypress-cucumber-preprocessor').default
const report = require('../../report');
const fs = require('fs');
const path = require('path');
const previousFeatures = {}
const navegatorInfoPath = "cypress/fixtures/read-write/navegadorAndSo.json"


function processFilesFromDir(dirPath, method = 'get') {
  fs.readdirSync(dirPath).forEach(file => {
    const fileInfo = fs.openSync(`${dirPath}/${file}`, 'r')
    if (fs.fstatSync(fileInfo).isDirectory()) {
      const newDirPath = `${dirPath}/${file}`
      fs.readdirSync(newDirPath).forEach(fileNested => {
        processFile(newDirPath, fileNested, method)
      });
    }
    else {
      processFile(dirPath, file, method)
    }
  });
}

function processFile(dirPath, fileName, method) {
  if (fileName.includes('.feature')) {
    if (method == 'get') {
      addExampleData(dirPath, fileName);
    } else {
      removeExampleData(dirPath, fileName);
    }
  }
}

function removeExampleData(dirPath, feature) {
  fs.writeFileSync(path.resolve(dirPath, feature), previousFeatures[feature], 'utf-8');
}

function addExampleData(dirPath, feature) {
  let fileFeature = '';
  let previousDataFeature = '';
  const linebreak = '\n';
  let exampleData = false
  let dataExample;
  const featureArray = fs.readFileSync(path.resolve(dirPath, feature), 'utf-8').split(/\r?\n/)
  featureArray.forEach((featureLine, index) => {
    previousDataFeature += featureLine;
    if (index != (featureArray.length - 1))
      previousDataFeature += linebreak
    if (featureLine.includes("@example@-")) {
      const dataTag = featureLine.split("-")[1].replace("|", "").trim()
      const dataPath = CSVMAP[dataTag]

      fs.readFileSync(path.resolve('cypress/fixtures/data', dataPath), 'utf-8').split(/\r?\n/).forEach((dataLine) => {
        let exampleDataLine = '| ';

        dataLine.split(',').forEach((item) => {
          exampleDataLine += item + ' | ';
        });

        if (dataLine.trim() != "") {
          fileFeature += exampleDataLine + linebreak;
        }
      });
      exampleData = false;
      if (!featureLine.startsWith("#"))
        featureLine = `#${featureLine}`
    }
    else if ((featureLine.trim().startsWith('@') || featureLine.trim().startsWith('Scenario') || index == featureArray.length - 1) && exampleData) {
      exampleData = false;
      fileFeature += dataExample + linebreak;
    }
    if (!exampleData) {
      fileFeature += featureLine + linebreak;
    }
    else {
      dataExample += featureLine + linebreak;
    }
    if (featureLine.includes("Examples")) {
      dataExample = ''
      exampleData = true
    }
  });

  previousFeatures[feature] = previousDataFeature;
  fs.writeFileSync(path.resolve(dirPath, feature), fileFeature, 'utf-8');
}

function updateBrowserInformation(browser) {
  const infoSO = JSON.parse(fs.readFileSync(navegatorInfoPath, 'utf-8'));
  infoSO.navegador = browser.name;
  fs.writeFileSync(navegatorInfoPath, JSON.stringify(infoSO), 'utf-8');
}

function updateSOInformation(system) {
  let Name = "Unknown OS";
  if (system.osName.indexOf("win") != -1) Name =
    "Windows OS";
  if (system.osName.indexOf("Mac") != -1) Name =
    "Macintosh";
  if (system.osName.indexOf("Linux") != -1) Name =
    "Linux OS";
  if (system.osName.indexOf("Android") != -1) Name =
    "Android OS";
  if (system.osName.indexOf("like Mac") != -1) Name =
    "iOS";
  const dataSO = `{"sistemaOperativo":"${Name}"}`
  fs.writeFileSync(navegatorInfoPath, dataSO, 'utf-8');
}

module.exports = (on, config) => {
  on('file:preprocessor', cucumber())

  on("before:run", (_details) => {
    console.log("============BEFORE-RUN============")
    updateSOInformation(_details.system)
    const cucumberJsonDir = path.resolve(process.cwd(), "cypress/cucumber-json");
    if (fs.existsSync(cucumberJsonDir)) {
      fs.rmdirSync(cucumberJsonDir, {
        recursive: true,
      });
    }
    specFolder = _details.config.specFolder
    processFilesFromDir(specFolder, 'get')
  })

  on('before:browser:launch', (_browser, _launchOptions) => {
    updateBrowserInformation(_browser)
  })

  on('after:run', (_results) => {
    console.log("============AFTER-RUN============")
    processFilesFromDir(specFolder, 'remove')
    report.doReportActivities();
  })

  return config;
}
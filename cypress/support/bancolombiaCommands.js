const Ajv = require("ajv");
const ajv = new Ajv();

/** Guarda una variable para ser reutilizada en steps diferentes.
   * Args:
       -[string] variableName: Nombre de la variable a guardar.
       -[*] value: Valor a almacenar.
  */
Cypress.Commands.add("saveVariable", (variableName, value) => {
  cy.wrap(value).as(variableName);
});

/** Valida que una página inlcuya en su título un texto predefinido y lo imprime. 
   * Args:
       - [string] textToValidate: Texto a validar en el título.
  */
Cypress.Commands.add("titleContainsText", (textToValidate) => {
  cy.title()
    .should("include", textToValidate)
    .then((title) => {
      cy.log(`Título de la página: ${title}`);
    });
});

/** Valida que una página inlcuya en su url un texto predefinido y lo imprime 
   * Args:
       - [string] textToValidate: Texto a validar en la url.
  */
Cypress.Commands.add("urlContainsText", (textToValidate) => {
  cy.url()
    .should("include", textToValidate)
    .then((url) => {
      cy.log(`Url de la página: ${url}`);
    });
});



Cypress.Commands.add("generateLetters", (numero, letra) => {
  const texto = letra.repeat(numero);
  return texto;
});

@HU_BC-001 @landing
Feature: Bancolombia Challenge QA Automation

  Background: I navigate to page
    Given I browse the page to automate

  @landing-cicles @happy-path @regression
  Scenario: Complete the 10 cycles successfully
    When I enter the credentials
    And I complete all 10 cycles
    Then I see a success tab with hash: "c2fe0e21ce445033MS00LTY0NzQzNw==df63afbf4da3d4b1"
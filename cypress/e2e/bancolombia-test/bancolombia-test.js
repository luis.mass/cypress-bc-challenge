import { Given, Then, When } from "cypress-cucumber-preprocessor/steps";
const math = require('mathjs');
require('cypress-xpath');


const loginPage = require("../../fixtures/page_objects/LoginPage.json")
const ciclePage = require("../../fixtures/page_objects/CiclePage.json")
const successPage = require("../../fixtures/page_objects/SuccessPage.json")


Given("I browse the page to automate", () => {
    cy.visit('/')
})

When("I enter the credentials", () => {
   cy.waitFor(cy.xpath(loginPage.labelTitleLogin).should('be.visible').and('include.text','Iniciar sesión'));
   cy.xpath(loginPage.inputUsername).type(Cypress.env('username'));
   cy.xpath(loginPage.inputPass).type(Cypress.env('password'));
   cy.xpath(loginPage.buttonSubmit).click();
   
});

When("I complete all {int} cycles", (NumeroCycles) => {
    for (let i = 1; i <= NumeroCycles; i++) {
        cy.waitFor(cy.xpath(ciclePage.labelTitleAssert).should('be.visible').and('include.text','Complete el siguiente formulario'));
        
        //select operación
        cy.xpath(ciclePage.labelOperation).invoke('text').then((operation) => {
            const cleanedOperation = operation.replace(/\s/g, '');
            const result = math.evaluate(cleanedOperation);
            cy.xpath(ciclePage.selectResponseOperation).select(result.toString());
        });
     

        //repetir letra
        cy.xpath(ciclePage.labelRepeatLetter).invoke('text').then((letter) =>{
            // Extraer el número y la letra usando una expresión regular
            const match = letter.match(/(\d+) veces la letra "(.*?)"/);
            const numero = match[1];  // Obtiene el número
            const letra = match[2];   // Obtiene la letra entre comillas
            cy.generateLetters(numero, letra).then(resultado => {
                cy.xpath(ciclePage.inputRepeatLetter).type(resultado,{ delay: 0 })
                cy.xpath(ciclePage.btnSubmit).click()
                //cy.wait(1000)
            });
        })
    }
});

Then("I see a success tab with hash: {string}", (hashToValidate) => {
    cy.waitFor(cy.xpath(successPage.labelTitleAssert).should('be.visible').and('include.text','Felicidades, has terminado la prueba exitosamente'));
    cy.urlContainsText("automatization/success/")
    cy.xpath(successPage.labelHashSuccess).invoke('text').then((hash) =>{
        expect(hash).to.include(hashToValidate);
        cy.log("hash de prueba exitosa: ", hash)
    })
});

